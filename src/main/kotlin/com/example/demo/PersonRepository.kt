package com.example.demo

import org.springframework.data.jpa.repository.JpaRepository

/**
 * @author Nasir Rasul {@literal nasir@mnrasul.com}
 */

interface PersonRepository : JpaRepository<Person, Long>