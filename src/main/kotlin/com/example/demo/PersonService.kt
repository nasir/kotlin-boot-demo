package com.example.demo

/**
 * @author Nasir Rasul {@literal nasir@mnrasul.com}
 */

interface PersonService{

    fun createPerson(person: Person): Person

    fun read(id: Long): Person

    fun delete(id: Long)
}