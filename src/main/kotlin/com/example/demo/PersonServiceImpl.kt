package com.example.demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

/**
 * @author Nasir Rasul {@literal nasir@mnrasul.com}
 */

@Service
class PersonServiceImpl : PersonService {
    @Autowired
    lateinit var personRepository: PersonRepository

    override fun createPerson(person: Person): Person {
        return personRepository.save(person)
    }

    override fun read(id: Long): Person {
        return personRepository.getOne(id)
    }

    override fun delete(id: Long) {
        personRepository.deleteById(id)
    }
}