package com.example.demo

import javax.persistence.Entity
import javax.persistence.Id

/**
 * @author Nasir Rasul {@literal nasir@mnrasul.com}
 */

@Entity
data class Person(@Id val id: Long, val name: String)