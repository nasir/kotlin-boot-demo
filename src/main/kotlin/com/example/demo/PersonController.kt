package com.example.demo

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.concurrent.atomic.AtomicLong

/**
 * @author Nasir Rasul {@literal nasir@mnrasul.com}
 */

@RestController
class PersonController {
    @Autowired
    lateinit var personService: PersonService

    val counter = AtomicLong()


    @GetMapping("/person")
    fun read(@RequestParam("id") id: Long): Person{
        return personService.read(id)
    }

    @PostMapping("/person")
    fun create(@RequestParam("name") name: String): Person{
        return personService.createPerson(Person(counter.getAndIncrement(), name))
    }

}